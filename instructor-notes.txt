  - Note: please actually check in a pdf in the future
  - Planning:
    - You blame the load imbalance on the pivot, but the more obvious reason
      for the load imbalance in quicksort with an uneven number of processes
      is that, by default, one process must take the upper half of two other
      process's keys in the exchange: -0.25
    - Good appeal to statistical analysis for pivot selection.  I found the
      line in your code about the necessary number of samples for a 5%
      confidence interval in median estimation.  That's great, but when you
      use something like that, please cite a source.  It's not an estimator
      I've encountered before, and knowing the assumptions placed on the
      distribution would be important.
