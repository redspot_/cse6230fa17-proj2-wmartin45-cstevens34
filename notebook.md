```
Here is our initial plan:

Compile and run the original code

determine test plan:
    run with a large number of nodes (256 for bitonic, 255 for quicksort)
    run single threaded versions
    capture trace-view profiling data
    note timings


analysis:
    timings offer a holistic view into which algorithms perform better
    current expectations are:
        bitonic > quicksort for multithreaded with equal-ish processors

initial analysis:
    we ran the original code with 7 threads to simplify analyzing the data.  Our initial conjecture based on looking at the trace-view (image initial-traceview.png) was that there appeared to be uneven work-loads during the sorting operation.  The initial thought was to balance out the sorting work-load and this inevitably led us to analyzing the ChoosePivot function.

Removed initial sort:
    we ran the original code but removed the inital sorting of the array at the top of the sort-recursive function.  This initial sort only provied overhead and no meaningful representation of a median which we though might by skewing our initial conjecture.
    The analysis showed by the traceview (traceview-removed-sort) did confirm some of this--the pivot calls are much more aligned which is what we want to see--meaning that all processes should be roughly choosing a pivot at the same time, otherwise that means that processors are doing too much work and hence lagging.
    This analysis was performed with 31 processors and you can more readily see that choosing a proper pivot is still causing some discrepencies--in this image the gray/green bands represent the times spent sorting and then calling the ChoosePivot function.  We want to see more lock-step type timing between processors of the same group (grouping can be more readily seen at the tail of the graph).
    Our coclusion for the next round of fixes is to examine better options for choosing a pivot

Improved Code:

    Based on above analysis we decided to use a sampling statistical method to estimate the median.   Our algorithm calculates an estimate from the local popoulation and then averages all the estimated medians.  This gives a decent approximation of the true median with almost 50/50 splits occuring as evidenced from the code.
    Our final trace analysis (traceview-sampling.png) shows that the processors are executing in a more rigorous lock-step fashion which is better since it indicates that few processes are lagging behind others.

Original code from professor, small and large:
[0] Tested numKeysLocal 10000, numKeysGlobal 310000, total bytes 2480000: average bandwidth 4.202455e+07
[0] Tested numKeysLocal 1000000, numKeysGlobal 31000000, total bytes 248000000: average bandwidth 6.524081e+06

Our code, small and large:
[0] Tested numKeysLocal 10000, numKeysGlobal 310000, total bytes 2480000: average bandwidth 1.437279e+08
[0] Tested numKeysLocal 1000000, numKeysGlobal 31000000, total bytes 248000000: average bandwidth 3.507378e+08

small: 1.437279e+08 / 4.202455e+07 = 3.42 = 342% faster

large: 3.507378e+08 / 6.524081e+06 = 53.76 = 537.6% faster
```
