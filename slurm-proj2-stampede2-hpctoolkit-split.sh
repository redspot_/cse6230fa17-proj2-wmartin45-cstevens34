#!/bin/sh
#SBATCH  -J proj2-wm                     # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 4                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:15:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH  -o wm-proj2-%j.out              # Standard output and error log

module load hpctoolkit
module load valgrind
ulimit -c unlimited
pwd; hostname
OUT=hpctoolkit-test_proj2-split-${SLURM_JOB_ID}
VLOG=valgrind-test_proj2-%p
small=10000
psmall=`expr 32 '*' $small`
large=1000000
plarge=`expr 32 '*' $large`

git checkout master
git rev-parse HEAD
git diff-files
make clean
make test_proj2
hpcstruct test_proj2
cp test_proj2.hpcstruct test_proj2.hpcstruct.master
date
ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-small-1 test_proj2 $psmall $psmall 64 0 1
date
ibrun -n 31 tacc_affinity hpcrun -t -o ${OUT}-small-31 test_proj2 $small $small 64 0 1
date
ibrun -n 32 tacc_affinity hpcrun -t -o ${OUT}-small-32 test_proj2 $small $small 64 0 1
date

ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-large-1 test_proj2 $plarge $plarge 64 0 1
date
ibrun -n 31 tacc_affinity hpcrun -t -o ${OUT}-large-31 test_proj2 $large $large 64 0 1
date
ibrun -n 32 tacc_affinity hpcrun -t -o ${OUT}-large-32 test_proj2 $large $large 64 0 1
date

git checkout project2
git rev-parse HEAD
git diff-files
make clean
make test_proj2
hpcstruct test_proj2
date
ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-small-1-r1 test_proj2 $psmall $psmall 64 0 1
date
ibrun -n 31 tacc_affinity hpcrun -t -o ${OUT}-small-31-r1 test_proj2 $small $small 64 0 1
date
ibrun -n 32 tacc_affinity hpcrun -t -o ${OUT}-small-32-r1 test_proj2 $small $small 64 0 1
date

ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-large-1-r1 test_proj2 $plarge $plarge 64 0 1
date
ibrun -n 31 tacc_affinity hpcrun -t -o ${OUT}-large-31-r1 test_proj2 $large $large 64 0 1
date
ibrun -n 32 tacc_affinity hpcrun -t -o ${OUT}-large-32-r1 test_proj2 $large $large 64 0 1
date

exit 0
if [ x"${1}" != x ]
then
nprocs="${1}"
echo nprocs=${nprocs}
ibrun -n ${nprocs} tacc_affinity \
	hpcrun -t -o ${OUT}-6-${nprocs} \
	test_proj2 \
	640000 16000000 2 0 2
	#valgrind \
	#--show-reachable=yes \
	#--suppressions=valgrind.supp \
	#--log-file=$VLOG \
	#test_proj2 \
	#80 100000000 64 0 5
else
ibrun tacc_affinity \
	hpcrun -t -o ${OUT}-6-${nprocs} \
	test_proj2 \
	640000 16000000 2 0 2
	#valgrind \
	#--show-reachable=yes \
	#--suppressions=valgrind.supp \
	#--log-file=$VLOG \
	#test_proj2 \
	#80 100000000 64 0 5
fi
date

#80 100000000 64 0 5
#70000 70000 64 0 1
#10000 10000 64 0 1
#20971517 20971517 64 0 1
#2995931 2995931 64 0 1
#640000 16000000 2 0 2
