#!/bin/sh
#SBATCH  -J proj2                        # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 4                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:10:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH  -o proj2-%j.out                 # Standard output and error log

git rev-parse HEAD
git diff-files
make test_proj2
pwd; hostname; date
module load hpctoolkit
#ibrun tacc_affinity hpcrun -t test_proj2 80 100000000 64 0 5
OUT=hpctoolkit-test_proj2-split-${SLURM_JOB_ID}
#ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-1 test_proj2 70000 70000 64 0 1
#date
#ibrun -n 7 tacc_affinity hpcrun -t -o ${OUT}-2 test_proj2 10000 10000 64 0 1
#date
#ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-3 test_proj2 20971517 20971517 64 0 1
#date
#ibrun -n 7 tacc_affinity hpcrun -t -o ${OUT}-4 test_proj2 2995931 2995931 64 0 1
#date
ibrun -n 1 tacc_affinity hpcrun -t -o ${OUT}-5 test_proj2 640000 16000000 2 0 2
#ibrun tacc_affinity hpcrun -t -o ${OUT}-6 test_proj2 10000 16000000 2 0 2
ibrun -n 255 tacc_affinity hpcrun -t -o ${OUT}-6 test_proj2 640000 16000000 2 0 2
