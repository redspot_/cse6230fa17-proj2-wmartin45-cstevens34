#define _POSIX_C_SOURCE 200112L
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include "proj2.h"

int Proj2Malloc(size_t size, void **array)
{
  int err;

  err = posix_memalign(array, 64, size); PROJ2CHK(err);
	if (err == EINVAL) {
		printf("posix_memalign:EINVAL\n");
		printf("Malloc: %p, %zu\n",array, size);
	} else if (err == ENOMEM) {
		printf("posix_memalign:ENOMEM\n");
		printf("Malloc: %p, %zu\n",array, size);
	}

  return 0;
}

int Proj2Free(void **array)
{
  free(*array);
  *array = NULL;
  return 0;
}

